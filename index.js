let paint = false;
let position = [0, 0];
let canvas = null;
let ctx = null;
let rect = null;
let model = null;
const classNames = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
let histogramDiv = null;

const main = async () => {
    // console.log("starting");
    model = await tf.loadLayersModel("js_model/model.json");
    const mainCanvas = document.querySelector(".mainCanvas");
    canvas = mainCanvas;
    ctx = canvas.getContext("2d");
    rect = canvas.getBoundingClientRect();
    canvas.addEventListener("mousedown", startPainting);
    canvas.addEventListener("mouseup", stopPainting);
    canvas.addEventListener("mousemove", sketch);
    histogramDiv = document.querySelector(".histogramDiv");
    document
        .querySelector(".predictButton")
        .addEventListener("click", getPrediction);
    document
        .querySelector(".clearButton")
        .addEventListener("click", clearCanvas);
};

function clearCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

async function getPrediction() {
    const canvasData = tf.browser
        .fromPixels(canvas)
        .resizeBilinear([28, 28])
        .mean(2)
        .toFloat()
        .div(255.0)
        .expandDims(0);
    canvasData.reshape([1, 28, 28]);
    // console.log(await canvasData.array());
    const prediction = model.predict(canvasData);
    const arr = (await prediction.array())[0];
    // console.log(arr);
    // const surface = tfvis.visor().surface({ name: "Barchart", tab: "Charts" });
    const data = [
        { index: 0, value: 50 },
        { index: 1, value: 100 },
        { index: 2, value: 150 },
    ];
    tfvis.render.barchart(
        histogramDiv,
        arr.map((v, i) => {
            // console.log(v);
            return {
                index: i,
                value: v,
            };
        }),
        {}
    );
}

function startPainting(event) {
    paint = true;
    position = getPosition(event);
}

function stopPainting() {
    paint = false;
    getPrediction();
}

function getPosition(event) {
    const x = event.clientX - rect.left;
    const y = event.clientY - rect.top;
    return [x, y];
}

function sketch(event) {
    if (!paint) return;
    ctx.beginPath();
    ctx.lineWidth = 10;
    ctx.lineCap = "round";
    ctx.strokeStyle = "green";
    let [x, y] = position;
    // console.log(x, y);
    ctx.moveTo(x, y);

    // The position of the cursor
    // gets updated as we move the
    // mouse around.
    position = getPosition(event);

    // A line is traced from start
    // coordinate to this coordinate
    ctx.lineTo(position[0], position[1]);

    // Draws the line.
    ctx.stroke();
    getPrediction();
    // console.log(1);
}

main();
