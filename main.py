import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt
import pathlib
import tensorflowjs as tfjs
import numpy as np

mnist = keras.datasets.mnist

(train_images, train_labels), (test_images, test_labels) = mnist.load_data()


print(train_images.shape)
print(len(train_labels))
print(train_labels)


# plt.figure()
# plt.imshow(train_images[0])
# plt.colorbar()
# plt.grid(False)
# plt.show()


train_images = train_images / 255.0

test_images = test_images / 255.0

train_images = np.expand_dims(train_images, -1)
test_images = np.expand_dims(test_images, -1)
# plt.figure(figsize=(10, 10))
# for i in range(25):
#     plt.subplot(5, 5, i+1)
#     plt.xticks([])
#     plt.yticks([])
#     plt.grid(False)
#     plt.imshow(train_images[i], cmap=plt.cm.binary)
#     plt.xlabel(train_labels[i])
# plt.show()


def build_model():
    inputs = keras.layers.Input(shape=(28, 28, 1))
    conv0 = keras.layers.Conv2D(
        32, kernel_size=(3, 3), activation="relu")(inputs)
    pool0 = keras.layers.MaxPooling2D(pool_size=(2, 2))(conv0)
    conv1 = keras.layers.Conv2D(
        64, kernel_size=(3, 3), activation="relu")(pool0)
    pool1 = keras.layers.MaxPooling2D(pool_size=(2, 2))(conv1)
    flat0 = keras.layers.Flatten()(pool1)
    drop0 = keras.layers.Dropout(0.5)(flat0)
    outputs = keras.layers.Dense(10, activation="softmax")(drop0)

    model = keras.Model(inputs=inputs, outputs=outputs)
    model.compile(optimizer='adam',
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(
                      from_logits=True),
                  metrics=['acc'])

    return model


model = build_model()
model.summary()
keras.utils.plot_model(model, "model.png")


model.fit(train_images, train_labels, epochs=20)
model.save('keras-model.h5')
tfjs.converters.save_keras_model(model, 'js_model')

test_loss, test_acc = model.evaluate(test_images,  test_labels, verbose=2)

print('\nTest accuracy:', test_acc)
